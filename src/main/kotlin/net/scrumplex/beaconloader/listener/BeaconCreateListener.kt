package net.scrumplex.beaconloader.listener

/*
 *    Beacon Loader
 *    Copyright (C) 2019 Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import net.scrumplex.beaconloader.BeaconLoader
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockPlaceEvent

class BeaconCreateListener(private val main: BeaconLoader) : Listener {

    @EventHandler
    fun on(e: BlockPlaceEvent) {
        val b = e.blockPlaced
        if (main.checkForBeaconLoader(b)) {
            main.activateBeacon(b, broadcast = true, save = true)
        }
    }

    @EventHandler
    fun on(e: BlockBreakEvent) {
        val b = e.block
        if (main.checkForBeaconLoader(b)) {
            main.deactivateBeacon(b, broadcast = true, save = true)
        }
    }
}
