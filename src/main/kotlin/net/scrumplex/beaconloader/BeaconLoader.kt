package net.scrumplex.beaconloader

/*
 *    Beacon Loader
 *    Copyright (C) 2019 Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import net.scrumplex.beaconloader.configuration.Config
import net.scrumplex.beaconloader.configuration.SerializableBeacon
import net.scrumplex.beaconloader.listener.BeaconCreateListener
import org.bukkit.Material
import org.bukkit.block.Block
import org.bukkit.block.BlockFace
import org.bukkit.configuration.serialization.ConfigurationSerialization
import org.bukkit.plugin.java.JavaPlugin
import java.util.function.Consumer

class BeaconLoader : JavaPlugin() {

    private lateinit var cfg: Config

    override fun onLoad() {
        ConfigurationSerialization.registerClass(SerializableBeacon::class.java)
    }

    override fun onEnable() {
        cfg = Config(this)

        server.pluginManager.registerEvents(BeaconCreateListener(this), this)

        cfg.getAllLoadingBeacons()
            .forEach(Consumer {
                activateBeacon(it)
            })

        server.scheduler.runTaskTimerAsynchronously(this, Runnable {
            cfg.getAllLoadingBeacons().forEach(Consumer {
                if (checkForBeaconLoader(it)) {
                    if (!it.chunk.isLoaded) {
                        logger.warning("Chunk at ${it.chunk.x} ${it.chunk.z} is not loaded, although BeaconLoader should keep it loaded!")
                        logger.warning("Please check if other plugins have unloaded the chunk.")
                        logger.warning("Trying to load chunk at ${it.chunk.x} ${it.chunk.z} again...")
                        activateBeacon(it)
                    }

                } else {
                    cfg.removeLoadingBeacon(it)
                    deactivateBeacon(it, broadcast = true, save = true)
                }
            })
        }, 200, 200)
    }

    override fun onDisable() {
        cfg.getAllLoadingBeacons().forEach(Consumer {
            deactivateBeacon(it)
        })
    }

    fun activateBeacon(block: Block, broadcast: Boolean = false, save: Boolean = false) {
        logger.info("Activating BeaconLoader at ${block.x} ${block.y} ${block.z} in world ${block.world.name}...")

        if (save)
            cfg.saveLoadingBeacon(block)

        block.world.loadChunk(block.chunk)
        block.chunk.isForceLoaded = true

        if (broadcast)
            server.broadcastMessage(cfg.getI18NString("broadcast.beacon.deactivated")!!)
        logger.info("Activated BeaconLoader at ${block.x} ${block.y} ${block.z} in world ${block.world.name}.")
    }


    fun deactivateBeacon(block: Block, broadcast: Boolean = false, save: Boolean = false) {
        logger.info("Deactivating BeaconLoader at ${block.x} ${block.y} ${block.z} in world ${block.world.name}...")

        if (save)
            cfg.removeLoadingBeacon(block)

        block.chunk.isForceLoaded = false
        if (block.chunk.isLoaded)
            block.world.unloadChunk(block.chunk) // try to unload chunk if not needed anymore (e.g. no active players)

        if (broadcast)
            server.broadcastMessage(cfg.getI18NString("broadcast.beacon.deactivated")!!)

        logger.info("Deactivated BeaconLoader at ${block.x} ${block.y} ${block.z} in world ${block.world.name}.")
    }


    fun checkForBeaconLoader(b: Block): Boolean {
        if (b.type == Material.BEACON) {
            val base = b.getRelative(BlockFace.DOWN)
            if (base.type == Material.DIAMOND_BLOCK) {
                if (
                    base.getRelative(BlockFace.NORTH).type == Material.GOLD_BLOCK &&
                    base.getRelative(BlockFace.NORTH_EAST).type == Material.GOLD_BLOCK &&
                    base.getRelative(BlockFace.EAST).type == Material.GOLD_BLOCK &&
                    base.getRelative(BlockFace.SOUTH_EAST).type == Material.GOLD_BLOCK &&
                    base.getRelative(BlockFace.SOUTH).type == Material.GOLD_BLOCK &&
                    base.getRelative(BlockFace.SOUTH_WEST).type == Material.GOLD_BLOCK &&
                    base.getRelative(BlockFace.WEST).type == Material.GOLD_BLOCK &&
                    base.getRelative(BlockFace.NORTH_WEST).type == Material.GOLD_BLOCK
                ) {
                    return true
                }
            }
        }
        return false
    }

}
