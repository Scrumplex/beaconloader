package net.scrumplex.beaconloader.configuration

/*
 *    Beacon Loader
 *    Copyright (C) 2019 Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import net.scrumplex.beaconloader.BeaconLoader
import org.bukkit.block.Block
import java.util.*

class Config(private val main: BeaconLoader) {

    private val fileConfiguration = main.config

    init {
        if (!fileConfiguration.contains("beacons"))
            fileConfiguration["beacons"] = mutableListOf<SerializableBeacon>()
        Defaults.defaultI18NStrings
            .forEach {
                if (!fileConfiguration.contains("i18n.${it.key}")) {
                    fileConfiguration["i18n.${it.key}"] = it.value
                }
            }
        save()
    }

    fun saveLoadingBeacon(block: Block) {
        val mutableList = fileConfiguration.getList("beacons") as MutableList<SerializableBeacon>
        mutableList.add(SerializableBeacon(block))

        save()
    }

    fun removeLoadingBeacon(block: Block) {
        val template = SerializableBeacon(block) // Create template object to compare against in the list
        val mutableList = fileConfiguration.getList("beacons") as MutableList<SerializableBeacon>
        mutableList.removeAll { it == template } // Unsure if overridden SerializableBeacon#equals() works with List#remove()

        save()
    }

    fun getAllLoadingBeacons(): List<Block> {
        val blocks = mutableListOf<Block>()

        val mutableList = fileConfiguration.getList("beacons") as MutableList<SerializableBeacon>
        mutableList
            .forEach {
                val block = main.server.getWorld(UUID.fromString(it.world))?.getBlockAt(it.x, it.y, it.z)
                if (block != null) {
                    blocks.add(block)
                }
            }

        return blocks
    }

    fun getI18NString(key: String): String? {
        return when {
            fileConfiguration.contains("i18n.$key") -> fileConfiguration.getString("i18n.$key")
            Defaults.defaultI18NStrings.containsKey(key) -> Defaults.defaultI18NStrings[key] // default value if non existent in config
            else -> null
        }
    }

    private fun save() {
        main.saveConfig()
    }


    private object Defaults {
        val defaultI18NStrings = mutableMapOf(
            Pair("broadcast.beacon.activated", "A Beacon Loader has been activated!"),
            Pair("broadcast.beacon.deactivated", "A Beacon Loader has been deactivated!")
        )
    }
}
