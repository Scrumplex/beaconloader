package net.scrumplex.beaconloader.configuration

/*
 *    Beacon Loader
 *    Copyright (C) 2019 Sefa Eyeoglu <contact@scrumplex.net> (https://scrumplex.net)
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

import org.bukkit.block.Block
import org.bukkit.configuration.serialization.ConfigurationSerializable

class SerializableBeacon(val x: Int, val y: Int, val z: Int, val world: String) : ConfigurationSerializable {

    constructor(block: Block) : this(block.x, block.y, block.z, block.world.uid.toString())

    constructor(map: Map<String, Any>) : this(map["x"] as Int, map["y"] as Int, map["z"] as Int, map["world"] as String)

    override fun serialize(): MutableMap<String, Any> {
        return mutableMapOf(
            Pair("x", x),
            Pair("y", y),
            Pair("z", z),
            Pair("world", world)
        )
    }

    override fun equals(other: Any?): Boolean {
        return when (other) {
            is SerializableBeacon -> {
                other.x == this.x &&
                        other.y == this.y &&
                        other.z == this.z &&
                        other.world == this.world
            }
            else -> false
        }
    }

    override fun hashCode(): Int {
        var result = x
        result = 31 * result + y
        result = 31 * result + z
        result = 31 * result + world.hashCode()
        return result
    }
}
