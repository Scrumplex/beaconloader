Beacon Loader
-------------
Spigot plugin to create chunk loading beacons

# Description
By installing this plugin your server gains the ability to give users access to chunk loaders.
It allows users to use beacons with a specific pattern as chunk loaders, which always load one chunk each.
This limitation was made to regulate the use of chunk loaders.

# Installation
Download the plugin from the Spigot Plugin Repository here (link pending).

You can always grab the latest nightly build from [GitLab CI](https://gitlab.com/Scrumplex/beaconloader/-/jobs/).

# Development
This project uses the Gradle build system. If your platform does not provide gradle in the package repositories you can easily use `./gradlew`

# License
This project is licensed under the GNU General Public License 3.0. You can read more about the LICENSE [here](LICENSE).
